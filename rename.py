# gtkrename - a simple file renamer
# Copyright (C) 2008,  Kristian Rumberg (kristianrumberg@gmail.com)

# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import os
import re
from sets import Set

class File:
    def __init__(self, filestr):
        self.filename = os.path.basename(filestr)
        self.dir = os.path.dirname(filestr)

    def _full_path(self):
        return os.path.join(self.dir, self.filename)

    def get_filename(self):
        return self.filename

    def invalid_filename(self):
        return len(self.filename) == 0

    def filename_matches(self, regexp_obj):
        if regexp_obj.search( self.get_filename() ):
            return True
        else:
            return False

    def mv(self, dest_file):
        os.rename(self._full_path(), dest_file._full_path())

    def file_exists(self):
        return os.path.exists(self._full_path())

    def sed_filename(self, regexp_obj, regexp_str_replace_with):
        newname = regexp_obj.sub(regexp_str_replace_with, self.filename)
        return File( os.path.join(self.dir, newname) )

class FileList:
    def __init__(self, filestrlist):
        self.filelist = []
        for fs in filestrlist:
            self.filelist.append( File(fs) )

    def grep(self, regexp_str):
        new_filelist = []
        try:
            regexp_obj = re.compile(regexp_str)
        except:
            return FileList([])

        for f in self.filelist:
            if f.filename_matches(regexp_obj):
                new_filelist.append(f)

        out_fl = FileList([])
        out_fl.filelist = new_filelist
        return out_fl

    def sed(self, regexp_str_search, regexp_str_replace_with):
        new_filelist = FileList([])
        if 0 == len(self.filelist):
            return new_filelist

        regexp_obj = re.compile(regexp_str_search)
        for f in self.filelist:
            new_filelist.filelist.append( f.sed_filename(regexp_obj, regexp_str_replace_with) )

        return new_filelist

    # returns True on success and False on failure
    def mv(self, dest_filelist, bool_simulate):
        srclist = self.filelist
        dstlist = dest_filelist.filelist

        # make sure all resulting filenames are unique
        if len(dstlist) != len(list(Set(dstlist))):
            return False

        for f in dest_filelist:
            if f.invalid_filename():
                return False
            elif f.file_exists():
                return False

        if False == bool_simulate:
            for s,d in zip(srclist, dstlist):
                s.mv(d)

        return True

    def __iter__(self):
        return self.filelist.__iter__()

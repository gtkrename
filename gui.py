# gtkrename - a simple file renamer
# Copyright (C) 2008,  Kristian Rumberg (kristianrumberg@gmail.com)

# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import sys
import gtk
import gobject
import gtk.glade

def fatal_error(msg):
    dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,gtk.MESSAGE_ERROR, gtk.BUTTONS_OK)
    dialog.set_markup(msg)
    dialog.run()
    dialog.destroy()
    sys.exit(-1)

class MainWindow:
    def _init_textview(self, view):
        view.set_model(gtk.TreeStore(gobject.TYPE_STRING))
        r = gtk.CellRendererText()
        p_column = gtk.TreeViewColumn("", r)
        p_column.add_attribute(r,"text",0)
        view.insert_column(p_column,0)
        view.set_rules_hint(True)

    def _update_textview_filelist(self, view, filelist):
        model = view.get_model()
        model.clear()
        for f in filelist:
            model.append(None, [f.get_filename()])
        view.expand_all()

    def _update_replaced_files_view_and_apply_button(self):
        self.out_filelist = self.in_filelist.sed( self.match_entry.get_text(), self.replace_entry.get_text() )
        self._update_textview_filelist(self.replaced_files_view, self.out_filelist)

        self.apply_button.set_sensitive( self.in_filelist.mv(self.out_filelist, True) )

    def _update_all(self):
        self.in_filelist = self.orig_filelist.grep( self.match_entry.get_text() )

        self._update_textview_filelist(self.matched_files_view, self.in_filelist)
        self._update_replaced_files_view_and_apply_button()

    def match_entry_changed(self, data):
        self._update_all()

    def replace_entry_changed(self, data):
        self._update_replaced_files_view_and_apply_button()

    def apply_button_clicked(self, data):
        self.in_filelist.mv(self.out_filelist, False)
        dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
        dialog.set_markup("Renaming finished :)")
        dialog.run()
        dialog.destroy()
        gtk.main_quit()

    def __init__(self, orig_filelist):
        self.orig_filelist = orig_filelist
        self.in_filelist = None
        self.out_filelist = None

        self.widgets = gtk.glade.XML("mainwindow.glade")
	self.window = self.widgets.get_widget("mainwindow")

        self.window.connect("delete-event", gtk.main_quit)
        self.window.show_all()

        self.match_entry = self.widgets.get_widget("match_entry")
        self.replace_entry = self.widgets.get_widget("replace_entry")
        self.matched_files_view = self.widgets.get_widget("matched_files_view")
        self.replaced_files_view = self.widgets.get_widget("replaced_files_view")
        self.cancel_button = self.widgets.get_widget("cancel_button")
        self.apply_button = self.widgets.get_widget("apply_button")

        self._init_textview(self.matched_files_view)
        self._init_textview(self.replaced_files_view)

        self._update_all()

        self.match_entry.connect("changed", self.match_entry_changed)
        self.replace_entry.connect("changed", self.replace_entry_changed)
        self.cancel_button.connect("clicked", gtk.main_quit)
        self.apply_button.connect("clicked", self.apply_button_clicked)

        gtk.main()

